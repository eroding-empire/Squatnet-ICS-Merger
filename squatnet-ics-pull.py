import requests
from bs4 import BeautifulSoup
 
 
url = 'https://radar.squat.net/en/groups/city/london'
reqs = requests.get(url)
soup = BeautifulSoup(reqs.text, 'html.parser')
 
urls = []
for link in soup.find_all('a'):
    urls.append((link.get('href')))
trueUrls = []
n = 0
length = len(urls)

while n < length:
    current = urls[n]
    #print(current[:11])
    if current[:11] == "/en/london/":
        current = "https://radar.squat.net/"  + current
        trueUrls.append(current)
        

    n = n + 1
print(trueUrls)

z = 1

try:
    while z < 3: #how many pages on london groups on squat .net + 1
        url = "https://radar.squat.net/en/groups/city/london?page=" + str(z)
        reqs = requests.get(url)
        soup = BeautifulSoup(reqs.text, 'html.parser')
         
        urls = []
        for link in soup.find_all('a'):
            urls.append((link.get('href')))
        n = 0
        length = len(urls)

        while n < length:
            current = urls[n]
            #print(current[:11])
            if current[:11] == "/en/london/":
                current = "https://radar.squat.net/"  + current
                trueUrls.append(current)
                

            n = n + 1
        z = z + 1


except TypeError:
    print("no more pages")
print(trueUrls)        
    

def findNode(url):
    reqs = requests.get(url)
    soup = BeautifulSoup(reqs.text, 'html.parser')
    n = 0
    hidden_tags = soup.find_all("input", type="hidden")
    for tag in hidden_tags:

        #print(tag)
        if tag.find("node") != -1:
            length1 = len(tag)
            tag = str(tag)
            new = tag[length1 - 9:]
            #print(new)
            last = new[0:6]
            if last[0] == "/":
                last = new[1:6]
            #print(new)
            n = n + 1
            if n == 2:
                break
    return(last)
nodes = []
for thing in trueUrls:
    #print(findNode(thing))
    nodes.append(findNode(thing))

print(nodes)
n = 0
length = len(nodes)
while n < length:
    urlCur = "https://radar.squat.net/ical/node/" + nodes[n]
    print(urlCur)
    r = requests.get(urlCur, allow_redirects=True)
    fileName = nodes[n] + ".ics"
    open(fileName, 'wb').write(r.content)
 
    n = n + 1
