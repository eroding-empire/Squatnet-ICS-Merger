REDUNDANT - Please use the Radar-API-City-Events-ICS script also found on this website.


# Squatnet-ICS-Merger

Python scripts to pull the iCal links for all groups in a city, and then merge them into one single ICS file for displaying on other websites.

__________________________________________________________

Instructions:

1. 
Clone repo or download files to a working folder you intend to run the scripts from.

2. 
Edit the file squatnet-ics-pull.py to scrape the city of your choosing.
Do this by editing lines 5, 19, 31, and 44 to remove "london" and replace with your chosen city.

3.
Run the pull script using the bash command:

```
$ python3 ./squatnet-ics-pull.py
```

You will see your working folder populates with a series of .ics files, one for each active group in your chosen city.

4.
Run the merge script using the bash command:

```
$ python3 ./squatnet-ics-merge.py
```

This will create a single file called "allEvents.ics".
This file can be used to upload to any calendar of your choosing, whether a wordpress calendar plugin, or your own personal calendar with an email provider.

__________________________________________________________

You may wish to run a cron job on the server you host this file on, to keep it updated regularly. Daily should suffice as events are not so frequent as to require more than this.

__________________________________________________________

Credit to Brengun for writing the whole damn thing :)