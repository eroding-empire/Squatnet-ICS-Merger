import os
def newList(start,end, target):
    n = start
    z = end
    newlist = []
    while n < z:
        newlist.append(target[n])
        n = n + 1
    print(newlist)
    return(newlist)


def getEvent(fileLocation):
    event = []
    file = open(fileLocation, "r", encoding="utf-8")

    #print(file.readlines())

    stripper = file.readlines()
    #print(stripper)
    n = 0
    stripperln = len(stripper)
    startInd = []
    endInd = []
    while n < stripperln:
        if stripper[n] == "BEGIN:VEVENT\n":
            startInd.append(n)
        elif stripper[n] == "END:VEVENT\n":
            y = n + 1
            endInd.append(y)
            
       
        n = n + 1
    z = len(startInd)
    x = 0
    while x < z:
        
        event = event + newList(startInd[x], endInd[x], stripper)
        x = x + 1
    #print(event)


    file.close()
    return(event)


files = os.listdir()
index = 0
eventsfile = []
while index < len(files):
    filename = files[index]
    if filename == "allEvents.ics":
        print("skipping allEvents")
    elif filename.endswith(".ics"):
        eventsfile.append(filename)

    index = index + 1
#eventsfile = ["346875.ics","396056.ics"] #list of all the ics files you want to combine
def allEvents(eventList):
    formattedEvents = []
    length = len(eventList)
    n = 0
    while n < length:
        newEvent = getEvent(eventList[n])
        formattedEvents = formattedEvents + newEvent
        n = n + 1
    #formattedEvents = stringify(formattedEvents)
    return(formattedEvents)


total = allEvents(eventsfile)

"""def stringify(lister):
    length = len(lister)
    n = 0
    new = ""
    while n < length:
        new = new + lister(n)
    new = str(new)
    return(new)"""
        
def newICS(content):
    start = ['BEGIN:VCALENDAR\n','VERSION:2.0\n']
    end = ['END:VCALENDAR\n']
    finish = start+content+end
    file = open("allEvents.ics", "w", encoding="utf-8")
    file.writelines(finish)
    file.close()

newICS(total)
